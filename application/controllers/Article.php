<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('aboutNews', 'newsdb');
        $this->load->helper('my_helper');
        if (!isset($this->session->company)) {
            $this->session->company = $this->newsdb->getCompanyInfo();
        }
        $this->companyInfo = $this->session->company;

        if (!isset($this->session->menues)) {
            $this->session->menues = $this->newsdb->getCats();
        }
        $this->menues = $this->session->menues;
    }
    

    public function userComment()
    {

        if (isset($_SESSION['user'])
            && $_SESSION['user']['loggedIn'] == true) 
        {

            $data['comment']['comment_text'] =
            sanitizeSting($_GET['comment']) ?? null;

            if ($data['comment']['comment_text']) 
            {
                $data['comment']['userID'] = $_SESSION['user']['id'];
                $data['comment']['post'] = $_SESSION['currentPost'];
                $save = $this->newsdb->saveComment($data);
            } else
            {
                //nothing will be stored as comment
            }
        } else {
            show_error('404');
        }
        redirect(base_url() .
            $_SESSION['currentPost']['cat'] . "/" .
            $_SESSION['currentPost']['sub'] . "/article/" .
            $_SESSION['currentPost']['postID'], 'refresh');

    }
    public function commentEdit($commentID){
        if($_SESSION['user']['loggedIn'])
        { 
            $data['comment'] = $this->newsdb->getComment((int)$commentID);
            $this->load->view("editComment",$data);
        } else {
            echo "Something went wrong";
        }
    }
    public function commentDeleteConfirm($commentID = 0,$userID = 0){
        $data['commentID'] = (int)$commentID;
        $data['userID'] = (int) $userID;
        $this->load->view(deleteConfirm, $data);
    }

    public function commentDelete($commentID=0, $userID=0){
        if($_SESSION['user']['loggedIn'])
        {   
            $validUser = $this->session->user['id'] == $userID ;
            if($validUser)
            {
                $delete = $this->newsdb->deleteComment($commentID, $_SESSION['user']['id']);
                if($delete)
                {
                    redirect(base_url() .
                        $_SESSION['currentPost']['cat'] . "/" .
                        $_SESSION['currentPost']['sub'] . "/article/" .
                        $_SESSION['currentPost']['postID'], 'refresh');
                } else {
                    echo "Something went wrong";
                }            
            }else {
                die("User is not authorized");
            }
        } else {
            die("Permission denied");
        }

    }

    public function updateComment(){

        $valid = $_SESSION['user']['loggedIn'];
        // $_SESSION['user']['id']==;
        
        if($valid){
            $submit = $this->input->post('submit')??null;
            $userID = (int)$this->input->post('userID')??null;
            $validUser = $_SESSION['user']['id'] == $userID ; //if user is owner of that comment

            if($submit && $validUser){
                $comment = $this->input->post('comment')??null;
                $commentID = $this->input->post('commentID')??null;
                if($comment && $commentID) {
                    $comment = sanitizeSting($comment);
                    $commentID = (int)$commentID;
                    $update = $this->newsdb->updateComment($commentID, $comment);
                    if($update){
                        redirect(base_url() .
                            $_SESSION['currentPost']['cat'] . "/" .
                            $_SESSION['currentPost']['sub'] . "/article/" .
                            $_SESSION['currentPost']['postID'], 'refresh');
                    } else {
                        echo "update went wrong";
                    }
                }
            } else {
                die ("Something went wrong.This is not your comment!");
            }
        } else {
            die("Access denied");
        }

    }


    ///////////////////////////////
}
