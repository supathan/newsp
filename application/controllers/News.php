<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News extends CI_Controller
{
    /**
    Variable from construct
    $this->session->company
    $this->session->menues
     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('aboutNews', 'newsdb');
        $this->load->helper('my_helper');

        if (!isset($this->session->company)) {
            $this->session->company = $this->newsdb->getCompanyInfo();
        }

        if (!isset($this->session->menues)) {
            $this->session->menues = $this->newsdb->getCats();
        }

        if (!isset($this->session->submenues)) {
            $this->session->submenues = $this->newsdb->getSubCats();
        }

        if (!isset($this->session->footer)) {
            $data['footer']['linksAway'] = $this->newsdb->getFooter('linksAway');
            $data['footer']['linksInner'] = $this->newsdb->getFooter('linksInner');
            $this->session->footer = $data['footer'];
        }
    }

    public function index()
    {
        $data['news'] = $this->newsdb->get6ForEachCat($_SESSION['menues']);
        $this->load->view('homepage', $data);
    }

    private function _sub($subcat, int $offset)
    {
        /**
        Functon for loading news for each catagory.
        e.g. bangladesh,international etc.
        (Subcatagory only)
         **/
        if (!isset($_SESSION['offset'])) {
            $_SESSION['offset'] = $offset;
        }
        switch ($offset) {
            case 1248:$offset = ++$_SESSION['offset'];
            break;
            case 8421:$offset = --$_SESSION['offset'];
            break;
            default:$_SESSION['offset'] = $offset;
        }

        if (--$offset < 0)
        // making it 0(0-11) 1(12-24) 2 3 4
        {
            $offset = $_SESSION['offset'] = 1;
        } else {
            $offset = $offset * 12;
        }

        $data['menues'] = $this->session->menues;
        $data['catInfo'] = $data['menues'][$this->uri->segment(1)];

//////// for page 123456 button
        $_SESSION['curLoc']['cat'] = $this->uri->segment(1);
        $_SESSION['curLoc']['sub'] = $this->uri->segment(2);
////////////////////
        $data['submenues'] = $this->session->submenues[$data['catInfo']['catID']];
        $data['subInfo'] = $data['submenues'][$subcat];

        $data['news'] = $this->newsdb->get12ItemForSub(
            $data['catInfo']['catEN'],
            $data['subInfo']['subcatEN'],
            $offset
        );

        $this->load->view('subpage', $data);
    }

    private function _subHome()
    {
        /**
        Functon for loading news for each catagory.
        e.g. bangladesh,international etc.
        (Catagory HOME only)
         **/
        $data['menues'] = $this->session->menues;
        $data['catInfo'] = getThisMenuInfo($this->session->menues, $this->uri->segment(1));
        $data['submenues'] = $this->session->submenues[$data['catInfo']['catID']];
        $data['news'] = $this->newsdb->get6ForCat($data['catInfo']['catEN']);
        /**
        Print_r($data['submenues']);exit;
         **/

        $this->load->view('catpage', $data);
    }

    public function article(string $cat, string $sub, string $newsID)
    {
//if i gets IDs as arguement, then make it as Name string
        if (is_numeric($cat) && is_numeric($sub) && is_numeric($newsID)) {
            /**
            Got NUMBER
             **/
            $catName = $this->newsdb->getCatname((int) $cat);
            $subcatName = $this->newsdb->getSubcatname((int) $cat, (int) $sub);
            $newsID = (int) $newsID;
            $url = base_url() . $catName . "/" . $subcatName . "/article/" . $newsID;

            header('Location: ' . $url);
            die();

            /**
            If cat and subcat as String
             **/
        } elseif ($this->_checkSubName($this->_checkCatName($cat), $sub)) {
            /**
            If cat and subcat is valid
             **/
            $this->_loadNews($cat, $sub, $newsID);
        } else {
            show_error("404");
        }

    }

    public function route(
        $cat,
        $subcat = '',
        $arg = 1
    ) {

        $catID = $this->_checkCatName($cat);
        /**
        If valid _checkCatName return ID
         **/
        if ($catID == true) {
            $subcheck = $this->_checkSubName($catID, $subcat);
            ($subcheck == true) ? $this->_sub($subcat, intval($arg)) : $this->_subHome();
        } else {
            show_error("404");
        }

    }

    private function _checkCatName(string $catName): int
    {
        /**
        Function to check if $catName is a valid catname
        if found then return ID as nonZero or FALSE to var
         **/
        $result;
        $cat = getThisMenuInfo($this->session->menues, $catName);
        if ($cat) {
            $result = $cat['catID'];
        } else {
            $result = 0;
        }
        return $result;
    }

    private function _checkSubName(int $catID, string $subcatName): bool
    {
        /**
        Function to check if subcatname is a valid name for that catagory
         **/
        $result;
        if (!$catID) {
            $result = false;
        } else {
            $result = $this->newsdb->checkSub($catID, $subcatName);
            /**
            Var is getting true or false only from db
             **/
        }

        return $result;
    }

    private function _loadNews($cat, $sub, $newsID)
    {
        /**
        Function for loading news when it is clicked
        And other news related to this single news.
         **/
        // session is required for posting comment from user
        $this->session->currentPost =
        array(
            "postID" => $newsID,
            "cat" => $cat,
            "sub" => $sub,
        );

        $data['catInfo'] = $this->session->menues[$this->uri->segment(1)];
        $data['submenues'] = $this->session->submenues[$data['catInfo']['catID']];
        $data['subInfo'] = $data['submenues'][$sub];
        $data['newsOne'] = $this->newsdb->getNewsOne($cat, $sub, $newsID);
        $data['newsOne']['reporter'] = $this->newsdb->getReporter((int)$data['newsOne']['user']);
        $data['comments'] = $this->newsdb->loadComment($_SESSION['currentPost']);
        $data['commentsCount'] = 0;

        if ($data['comments'] != null) {
            foreach ($data['comments'] as &$comment) {
                $comment['userInfo'] =
                $this->newsdb->getUserInfo($comment['user_id']);
                $data['commentsCount']++;
            }

        }
        
        $this->load->view('article', $data);

    }



}
