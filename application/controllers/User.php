<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
  private $info = array();
  private $dbd;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('aboutNews', 'newsdb');
    $this->load->model('persons_Model', 'userdb');
    $this->load->helper('my_helper');
    if (!isset($this->session->company)) {
        $this->session->company = $this->newsdb->getCompanyInfo();
    }
    $this->companyInfo = $this->session->company;

    if (!isset($this->session->menues)) {
        $this->session->menues = $this->newsdb->getCats();
    }
    $this->menues = $this->session->menues;

}

   /* public function __set($key, $value)
    {
        if (array_key_exists($key, $this->info)) {
            $value = sanitizeSting($value);
            $this->info[$key] = $value;
        } else {
            echo "Error: Person object doesn't have : " . $key;
            exit;
        }

    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->info)) {
            return $this->info[$key];
        } else {
            return null;
        }

    }

    public function showInfo()
    {
        if (empty($this->info)) {
            echo "No info loaded";
        } else {
            print_r($this->info);
        }

    }

  

    private function _loadKeys()
    {
        $fields = $this->db->getFields();
        if ($fields) {
            foreach ($fields as $key) {
                $this->info[$key] = null;
            }
        } else {
            die("Object not loaded with keys");
        }

    }

    public function load(int $id = null)
    {
        $info = $this->db->getInfo($id);
        if (!empty($info) || !$id) {
            foreach ($info as $key => $value) {
                $this->info[$key] = sanitizeSting($value);
            }

        } else {
            echo "No Person there";
        }
    }

    public function save()
    {
        if (!empty($this->info)) {
            $this->db->saveInfo($this->info);
        } else {
            die("Nothing to save");
        }
    }
*/

    // }

    // public function __get($key)
    // {
    //     if (array_key_exists($key, $this->info)) {
    //         return $this->info[$key];
    //     } else {
    //         return null;
    //     }

    // }
    private function save()
    {
        //it will save values to database from User object
        if (!empty($this->info)) {
            if($this->userdb->saveInfo($this->info))
                return true;
            else 
                return false;
        } else {
            return null;
        }
    }

    private function userInfo()
    {   
        // It will return UserInfo variable with data 
        // if there are values
        // else return null
        if (!$this->info) {
            return null;
        } else {
            return $this->info;
        }

    }

    private function _loadKeys()
    {
        //it will load Keys of user object from database field
        //and will set to null as default value
        $fields = $this->userdb->getFields();
        if ($fields) {
            foreach ($fields as $key) {
                $this->info[$key] = null;
            }
        } else {
            die("Object not loaded with keys");
        }

    }

    private function load(int $id = null)
    {
        //this will load values to User Object from database field
        $info = $this->userdb->getInfo($id);
        if (!empty($info) || !$id) {
            foreach ($info as $key => $value) {
                $this->info[$key] = $value;
            }

        } else {
            $this->info = null ;
        }
    }

    public function profile() {
        //show profile
        if(isset($_SESSION['user']['loggedIn'])){
            $id = $_SESSION['user']['id'];
            $this->load($id); // load data from database
            $this->session->userInfo = $this->userInfo();
            $this->load->view('user', $data);
            unset($this->session->userInfo);
        }else {
            header("Location:".base_url());
        }
    }

    public function saveInfo() 
    {
        if($_SESSION['user']['loggedIn'])
        {

            $this->info = null; //reset
            if($_FILES['profilePic']['name']){
                $imageMaxSize = 100000;
                
                if($_FILES['profilePic']['size'] < $imageMaxSize){
                    $type = explode("/",$_FILES['profilePic']['type'])[1];
                    if($type=="png" || $type=="jpg" || $type=="jpeg" ){
                        $this->info['image'] =  file_get_contents(
                            $_FILES['profilePic']['tmp_name']
                        );
                    } else {
                        $_SESSION['profileMsg']['image'] = "Image type should be jpg or png";
                        $this->profile();
                    }

                } else {
                    $_SESSION['profileMsg']['image'] = "Image size should be less than 200KB";
                    $this->profile();
                }

            }
            if(!$_SESSION['profileMsg']){
                $this->info['id'] = (int)sanitizeSting($_POST['userID']);
                $this->info['fname'] = sanitizeSting($_POST['firstName']);
                $this->info['lname'] = sanitizeSting($_POST['lastName']);
                $this->info['nick'] = $this->info['fname']." ".$this->info['lname'];
                $this->info['email'] = sanitizeSting($_POST['email']);
                $this->info['phon'] = sanitizeSting($_POST['phon']);
                $this->info['profile'] = sanitizeSting($_POST['profileOne']);

                if($this->save()){
                    $this->session->profileMsg['save'] = "Profile saved successfully";
                } else 
                {
                    $this->session->profileMsg['save'] = "Profile save error";
                }
                $this->profile();
            }
        }
        else {
            header("Location:".base_url());

        }

    }
}
