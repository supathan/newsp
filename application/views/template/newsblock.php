<?php

?>
<div class="col-1-3">
	<div class="wrap-col">
		<div class="box-item">

			<div class="card text-black bg-white" style="border:none">
				<div class="card-header">
<?php if($item['image']): ?>
					<img style="height:200px;width:400px" src="data:image/jpeg;base64,<?=base64_encode($item['image'])?>"/>
<?php else: ?>
				<img style="height:200px;width:400px" src="<?=base_url()?>img/demo.jpg"/>
<?php endif ?>

				</div>
				<div class="card-body">
					<h4 class="card-title" style="height: 40px;font-size:120%"><?=$item['title']?></h4>
					<p class="card-text" style="font-size:90%"><?=$item['content']?></p>
				</div>
				<a href="<?=base_url() . $item['cat_id'] . '/' . $item['subcat_id'] . '/article/' . $item['id']?>" class="btn btn-white btn-sm" style="margin-left:50%"><b>বিস্তারিত</b></a>
			</div>

		</div>
	</div>
</div>



<!--
<div class="col-1-3">
	<div class="wrap-col">
		<div class="box-item">
			<div class="card mb-3" style="max-width: 20rem;">
				<img src="<?=base_url() . $item['image']?>" alt="">
				<div class="row box-item">
					<h2><?=$item['title']?></h2>
				</div>
				<div class="row box-item">
					<p><?=$item['content']?></p>
				</div>
				<a href="<?=base_url() . $item['cat_id'] . '/' . $item['subcat_id'] . '/article/' . $item['id']?>" class="btn btn-primary">বিস্তারিত</a>

			</div>
		</div>
	</div>
</div> -->