<?php
$email = sanitizesting($_SESSION['forgot']['email']);
$token = sanitizesting($_SESSION['forgot']['token']);
?>

<style>
.container {
	margin-left : -280px;
  margin-bottom: 300px;
}

.form-control {
	border:none;
}
.form-control > input {
	border :1px solid yellow;
	border-radius: 15%;
	padding-top: 6px;
  padding-left: 20px;
	padding-bottom: 5px;
	width:0px;
}
#alert-danger {
  display :none;
  position: relative;
  width : 100%;
  margin: 10px 5px;
}
.alert {
  text-align: center;
}
</style>


<body>
  <?php
if (isset($_SESSION['error'])) {
  ?>
<div id="alert-danger" class="alert alert-dismissible alert-danger">
  <?php echo $_SESSION['error'] . "<br>";
  ?>
</div>
<?php
$_SESSION['error'] = null;
}
?>


  <?php
if (isset($_SESSION['notice']['success'])) {
  ?>
<div id="alert-success" class="alert alert-dismissible alert-success">
  <?php echo $_SESSION['notice']['success'] . "<br>";
  ?>
</div>
<?php
$_SESSION['notice']['success'] = null;
}
?>
<?php
if (isset($_SESSION['notice']['error'])) {
  ?>
<div id="alert-danger" class="alert alert-dismissible alert-danger">
  <?php echo $_SESSION['notice']['error'] . "<br>";
  ?>
</div>
<?php
$_SESSION['notice']['error'] = null;
}
?>


	<div class="container" >

  <form action="<?=base_url()?>guard/newPassword" method="POST"" method="POST">
    <div class="form-group row">
      <label for="password" id="passLabel" class="col-sm-2 col-form-label"> নতুন চাবি </label>
      <div class="col-sm-10">
        <div class="form-control">
          <input type="password" class="form-control" id="password" name="password" >
        </div>
      </div>
    </div>
    <div class="form-group row">
      <label for="passwordConfirm" class="col-sm-2 col-form-label"> আবার </label>
      <div class="col-sm-10">
        <div class="form-control">
        <input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm">
      </div>
      </div>
    </div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
          <div class="form-control">
            <input type="hidden" name="email" value="<?=$email?>"/>
            <input type="hidden" name="token" value="<?=$token?>"/>
            <input type="submit" id="submit" class="btn btn-primary" value=" ঠিক আছে "> </input>
          </div>
      </div>
    </div>
  </form>
</div>
<!-- <div id="circle">hello</div>
 -->
<script type="text/javascript">
$(".container").animate({marginTop:"40px",marginLeft : "15%"} , 600);
$(".form-control>input").animate({width:"60%"} , 700);
$("#alert-danger").fadeIn("slow");

</script>